use lox::IssuerPubKey;
use lox::proto;
use lox::cred::{Lox, Invitation};
use lox::bridge_table::{ENC_BUCKET_BYTES, BridgeLine};
use serde::{Deserialize, Serialize};
use serde_with::{serde_as};

#[derive(Deserialize, Serialize)]
pub struct OpenReqState {
    pub request: proto::open_invite::Request,
    pub state:proto::open_invite::State,
}

#[derive(Deserialize, Serialize)]
pub struct TrustReqState {
    pub request: proto::trust_promotion::Request,
    pub state:proto::trust_promotion::State,
}

#[derive(Deserialize, Serialize)]
pub struct MigReqState {
    pub request: proto::migration::Request,
    pub state:proto::migration::State,
}

#[derive(Deserialize, Serialize)]
pub struct LevelupReqState {
    pub request: proto::level_up::Request,
    pub state:proto::level_up::State,
}

#[derive(Deserialize, Serialize)]
pub struct IssueInviteReqState {
    pub request: proto::issue_invite::Request,
    pub state:proto::issue_invite::State,
}

#[derive(Deserialize, Serialize)]
pub struct RedeemReqState {
    pub request: proto::redeem_invite::Request,
    pub state:proto::redeem_invite::State,
}

#[derive(Deserialize, Serialize)]
pub struct CheckBlockageReqState {
    pub request: proto::check_blockage::Request,
    pub state:proto::check_blockage::State,
}

#[derive(Deserialize, Serialize)]
pub struct BlockageMigReqState {
    pub request: proto::blockage_migration::Request,
    pub state:proto::blockage_migration::State,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PubKeys {
    pub lox_pub: IssuerPubKey,
    pub migration_pub: IssuerPubKey,
    pub migrationkey_pub: IssuerPubKey,
    pub reachability_pub: IssuerPubKey,
    pub invitation_pub: IssuerPubKey,
}

#[serde_as]
#[derive(Serialize, Deserialize)]
pub struct EncBridgeTable {
   #[serde_as(as = "Vec<[_; ENC_BUCKET_BYTES]>")]
    pub etable: Vec<[u8; ENC_BUCKET_BYTES]>,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct LoxCredential {
    pub lox_credential: Lox,
    pub bridgeline: Option<BridgeLine>,
    pub invitation: Option<Invitation>,
}
