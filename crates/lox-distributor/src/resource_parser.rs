use lox::bridge_table::{BridgeLine, BRIDGE_BYTES};
use rdsys_backend::proto::Resource;

pub fn parse_resource(resource: Resource) -> BridgeLine {
    let mut ip_bytes: [u8; 16] = [0; 16];
    ip_bytes[..resource.address.len()].copy_from_slice(resource.address.as_bytes());
    let resource_uid = resource
        .get_uid()
        .expect("Unable to get Fingerprint UID of resource");
    let infostr: String = format!(
 "type={} blocked_in={:?} protocol={} fingerprint={:?} or_addresses={:?} distribution={} flags={:?} params={:?}",
 resource.r#type,
 resource.blocked_in,
 resource.protocol,
 resource.fingerprint,
 resource.or_addresses,
 resource.distribution,
 resource.flags,
 resource.params,
);
    let mut info_bytes: [u8; BRIDGE_BYTES - 26] = [0; BRIDGE_BYTES - 26];

    info_bytes[..infostr.len()].copy_from_slice(infostr.as_bytes());
    BridgeLine {
        addr: ip_bytes,
        port: resource.port,
        uid_fingerprint: resource_uid,
        info: info_bytes,
    }
}
